import React from 'react'
import { Link } from 'react-router-dom'
import './css/404.css'

const Error404 = () => (
    <div id="">
        <p id="p404">404 <br/> Page Not Found</p>
        <center><Link id="a404" to="/home">HOME</Link></center>
    </div>
)
export default Error404