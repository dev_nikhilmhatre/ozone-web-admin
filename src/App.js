import React, { Component, Fragment } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { withCookies } from 'react-cookie';

import Login from './Components/Login/Login'
import Dashboard from './Components/Dashboard/Dashboard'
import BrandCategoryFlavour from './Components/BrandCategoryFlavour/BrandCategoryFlavour'
import ProductClient from './Components/ProductClient/ProductClient';
import Billing from './Components/Billing/Billing'
import AddEditProduct from './Components/ProductClient/AddEditProduct'
import Erro404 from './404'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


class App extends Component {
  render() {
    return (
      <Fragment>
        <BrowserRouter basename="/admin" >
          <Fragment>
            <Switch>
              <Route exact path='/' render={() => (<Login cookies={this.props.cookies} />)} />
              <Route exact path='/home' render={() => (<Dashboard cookies={this.props.cookies} />)} />
              <Route exact path='/brand-category-flavour' render={() => (<BrandCategoryFlavour cookies={this.props.cookies} />)} />
              <Route exact path='/product-client' render={() => (<ProductClient cookies={this.props.cookies} />)} />
              <Route exact path='/billing' render={() => (<Billing cookies={this.props.cookies} />)} />
              <Route exact path='/add-product' render={() => (<AddEditProduct cookies={this.props.cookies} />)} />
              <Route exact path='/edit-product/:id' render={(routeProps) => (<AddEditProduct {...routeProps} cookies={this.props.cookies} />)} />
              <Route exact component={Erro404} />
            </Switch>
          </Fragment>
        </BrowserRouter>
        <ToastContainer />
      </Fragment>
    );
  }
}

export default withCookies(App);
