export const addNew = (url, data, token) => {
    
    return fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Token ${token}`
        }
    }).then(res => res.json())
}

export const updateData = (url, data, token) => {
    return fetch(url, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Token ${token}`
        }
    }).then(res => res.json())
}

export const getData = (url, token) => {
    if (token === '') {
        return fetch(url).then(res => res.json())
    } else {
        const header = { headers: { 'Authorization': `Token ${token}` } }
        return fetch(url, header).then(res => res.json())
    }

}

export const login = (url, data) => {
    return fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
}

export const logout = (url, token) => {
    return fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': `Token ${token}`
        }
    })
        .then(res => res.json())
}


export const uploadFile = (url, token, data) => {
    return fetch(url, {
        method: 'POST',
        body: data,
        headers: {
            'Authorization': `Token ${token}`
        }
    })
        .then(res => res.json())
}