import React from 'react'
import { Redirect } from 'react-router-dom'
import { login } from '../../Common/apiCalls'
import { base_url } from '../../Common/Constants'
import { FormControl, FormGroup, ControlLabel, Button } from 'react-bootstrap'
import { toast } from 'react-toastify'

class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            login: false
        }
    }

    componentWillMount = () => {
        const { cookies } = this.props
        const token = cookies.get('token')
        if (token !== '' && token !== undefined) {
            this.setState({ login: true })
        } else {
            this.setState({ login: false })
        }
    }

    _onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    _onClick = () => {
        this._login()
    }

    _login = () => {
        const { cookies } = this.props
        login(`${base_url}/api-auth/login/`, {
            username: this.state.username,
            password: this.state.password
        }).then(res => {
            debugger
            if (res.login) {
                let d = new Date();
                d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
                let expires = d;
                cookies.set('token', res.token, {
                    path: '/',
                    expires
                })
                this.setState({ login: true })
            }else{
                toast.error(`Invalid credentials`, {
                    position: toast.POSITION.BOTTOM_RIGHT,
                    hideProgressBar: true
                })
            }
        })
    }

    _onKeyPress = (e) => {
        if (e.key === 'Enter') { this._login() }
    }

    render() {
        if (this.state.login) {
            return <Redirect to='/home' />
        } else {
            return (
                <div className='login'>
                    <div className='login-wrap'>
                        <h2 id="h2">Login</h2>
                        <div style={{ textAlign: 'center' }}>
                            <FormGroup style={{ textAlign: 'left' }}>
                                <ControlLabel>Username</ControlLabel>
                                <FormControl
                                    onKeyPress={this._onKeyPress}
                                    onChange={this._onChange}
                                    value={this.state.username}
                                    placeholder="Username" name="username" />
                            </FormGroup>

                            <FormGroup style={{ textAlign: 'left' }}>
                                <ControlLabel>Password</ControlLabel>
                                <FormControl
                                    type="password"
                                    onKeyPress={this._onKeyPress}
                                    onChange={this._onChange} value={this.state.password}
                                    placeholder="Password" name="password" />
                            </FormGroup>


                            <Button
                                style={{ background: '#2196f3', color: 'white' }}
                                onKeyPress={this._onKeyPress}
                                onClick={this._onClick}>
                                Sign in
                            </Button>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default Login