import React from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'
import { LinkContainer } from 'react-router-bootstrap'
import { Redirect } from 'react-router-dom'
import { logout } from '../Common/apiCalls'
import { base_url } from '../Common/Constants'

class NavBar extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            login: true
        }
    }

    componentWillMount = () => {
        const { cookies } = this.props
        const token = cookies.get('token')
        if (token !== '' && token !== undefined) {
            this.setState({ login: true })
        } else {
            this.setState({ login: false })
        }
    }

    _onLogoutClick = () => {
        const { cookies } = this.props
        const token = cookies.get('token')

        logout(`${base_url}/api-auth/logout/`, token)

        cookies.set('token', '', {
            path: '/'
        })
        this.setState({ login: false })
    }

    render() {
        if (this.state.login) {
            return (
                <Navbar collapseOnSelect inverse style={{ borderRadius: 0 }}>

                    <Navbar.Header>
                        <Navbar.Brand>
                            <NavLink to="/home" >Ozone</NavLink>
                        </Navbar.Brand>

                        <Navbar.Toggle />

                    </Navbar.Header>

                    <Navbar.Collapse>
                        <Nav>
                            <LinkContainer to='/brand-category-flavour'>
                                <NavItem>
                                    Brands | Categories | Flavours
                            </NavItem>
                            </LinkContainer>

                            <LinkContainer to='/product-client'>
                                <NavItem>
                                    Products | Clients
                            </NavItem>
                            </LinkContainer>

                            <LinkContainer to='/billing'>
                                <NavItem>
                                    Billing
                            </NavItem>
                            </LinkContainer>
                        </Nav>

                        <Nav pullRight onClick={this._onLogoutClick}>
                            {/* <LinkContainer to='/'> */}
                            <NavItem>
                                Logout
                            </NavItem>
                            {/* </LinkContainer> */}
                        </Nav>

                    </Navbar.Collapse>
                </Navbar>
            )
        } else {
            return <Redirect to='/' />
        }

    }
};

export default NavBar;