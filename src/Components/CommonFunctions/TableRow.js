import React from 'react'
import { base_url } from '../../Common/Constants'
import { updateData } from '../../Common/apiCalls'
import { FormControl, Button } from 'react-bootstrap'
import { toast } from 'react-toastify';

export const _tr = (item, type, onChange, cookie) => {
    const attr = {
        id: item.id.toString(),
        name: type,
        onChange: (e) => _onChange(e, onChange, cookie)
    }

    return (
        <tr key={item.id}>
            <td>
                <FormControl
                    placeholder="Name"
                    type="text"
                    {...attr}
                    onBlur={(e) => _onBlured(e, cookie)}
                    value={item.name}
                    style={{ border: 'none', boxShadow: 'none', background: 'white' }} />
            </td>

            {
                type === 'Brands' ?
                    <td>
                        <FormControl
                            value={item.country}
                            {...attr}
                            componentClass="select">
                            <option value="India">India</option>
                            <option value="Other">Other</option>
                        </FormControl>
                    </td>
                    : null
            }

            <td style={{ textAlign: 'center' }}>
                {
                    item.id === ''
                        ? <Button onClick={() => console.log('clicked')}>Add</Button>
                        : (
                            <div className="pretty p-switch" style={{ marginTop: 10 }}>
                                <input type="checkbox"
                                    {...attr}
                                    checked={item.is_disabled} />
                                <div className="state">
                                    <label></label>
                                </div>
                            </div>
                        )
                }
            </td>
        </tr>
    )
}


export const _onChange = (e, onChange, cookie) => {
    const target = e.target

    onChange({
        id: parseInt(target.id),
        inputType: target.type,
        value: target.type === 'checkbox' ? target.checked : target.value,
        arrayToTarget: target.name
    })

    if (target.type === 'checkbox') {
        let url = ''
        let msg = ''

        if (target.name === 'Brands') {
            msg = 'Brand'
            url = `${base_url}/api-brand/delete-a/${target.id}/`
        } else if (target.name === 'Categories') {
            msg = 'Category'
            url = `${base_url}/api-category/delete-a/${target.id}/`
        } else if (target.name === 'Flavours') {
            msg = 'Flavour'
            url = `${base_url}/api-flavour/delete-a/${target.id}/`
        } 

        let type = target.checked ? 'disabled' : 'enabled'
        msg += ` ${type} successfully`

        updateData(url, { is_disabled: target.checked }, cookie).then(res => {
            toast.info(`${msg}`, {
                position: toast.POSITION.BOTTOM_RIGHT,
                hideProgressBar: true
            })
        }).catch(err => {
            toast.error(`Something went wrong, Please try again later.`, {
                position: toast.POSITION.BOTTOM_RIGHT,
                hideProgressBar: true
            })
        })
    }
    else if (target.type === 'select-one') {
        let url = `${base_url}/api-brand/update-a/${target.id}/`
        updateData(url, { country: target.value }, cookie).then(res => {
            let msg = 'Brand updated Successfully'
            toast.info(`${msg}`, {
                position: toast.POSITION.BOTTOM_RIGHT,
                hideProgressBar: true
            })
        }).catch(err => {
            toast.error(`Something went wrong, Please try again later.`, {
                position: toast.POSITION.BOTTOM_RIGHT,
                hideProgressBar: true
            })
        })
    }
}

export const _onBlured = (e, cookie) => {
    const target = e.target
    let url = ''
    let msg = ''
    if (target.name === 'Brands') {
        msg = 'Brand'
        url = `${base_url}/api-brand/update-a/${target.id}/`
    } else if (target.name === 'Categories') {
        msg = 'Category'
        url = `${base_url}/api-category/update-a/${target.id}/`
    } else if (target.name === 'Flavours') {
        msg = 'Flavour'
        url = `${base_url}/api-flavour/update-a/${target.id}/`
    } 
    msg += ` updated Successfully`

    updateData(url, { name: target.value }, cookie).then(res => {
        toast.info(`${msg}`, {
            position: toast.POSITION.BOTTOM_RIGHT,
            hideProgressBar: true
        })
    }).catch(err => {
        toast.error(`Something went wrong, Please try again later.`, {
            position: toast.POSITION.BOTTOM_RIGHT,
            hideProgressBar: true
        })
    })
}

