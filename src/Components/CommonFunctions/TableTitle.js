import React from 'react'
import { Redirect } from 'react-router-dom'
import { Row, Col, Button, FormControl, Well } from 'react-bootstrap'
import { base_url } from '../../Common/Constants'
import { actions } from '../../Redux/AllActions'
import { addNew } from '../../Common/apiCalls'
import { toast } from 'react-toastify'

class TableHeader extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            country: 'Select',
            display: 'none',
            contact: '',
            email: '',
            type: this.props.name,
            redirect: false
        }
    }

    _onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    _onClick = () => {

        if (this.state.name.length <= 0) {
            toast.warn(`Name can not be empty`, {
                position: toast.POSITION.BOTTOM_RIGHT,
                hideProgressBar: true
            })
            return
        }

        let url = ''
        let data = {}
        let action = ''
        const cookie = this.props.cookies.get('token')
        let msg = ''
        if (this.state.type === 'Brands') {
            if (this.state.country === 'Select') {
                toast.warn(`Please select valid option from dropdown.`, {
                    position: toast.POSITION.BOTTOM_RIGHT,
                    hideProgressBar: true
                })
                return
            }
            msg = 'Brand'
            url = `${base_url}/api-brand/create-a/`
            data = {
                name: this.state.name,
                country: this.state.country
            }
            action = actions.newBrand
        } else if (this.state.type === 'Categories') {
            msg = 'Category'
            url = `${base_url}/api-category/create-a/`
            data = {
                name: this.state.name
            }
            action = actions.newCategory
        } else if (this.state.type === 'Flavours') {
            msg = 'Flavour'
            url = `${base_url}/api-flavour/create-a/`
            data = {
                name: this.state.name
            }
            action = actions.newFlavour
        } else if (this.state.type === 'Clients') {
            msg = 'Client'
            url = `${base_url}/api-client/list-create-a/`
            data = {
                ...this.state
            }
            action = actions.newClient
        }

        msg += ' added successfully'
        addNew(url, data, cookie).then(res => {
            if (res.id !== undefined) {
                this.props.addNew(action, res)
                this.setState({ name: '', country: 'Select', contact: '', email: '', })
                toast.info(`${msg}`, {
                    position: toast.POSITION.BOTTOM_RIGHT,
                    hideProgressBar: true
                })
            }

        }).catch(err => {
            toast.error(`Something went wrong, Please try again later.`, {
                position: toast.POSITION.BOTTOM_RIGHT,
                hideProgressBar: true
            })
        })

    }

    _redirect = () => {
        // console.log('redirect');
        this.setState({ redirect: true })
    }

    render() {
        const props = this.props
        const col = props.name === 'Brands' ? 5 : props.name === 'Clients' ? 3 : 8
        const colForSelect = window.innerWidth >= 1024 ? 4 : 3

        if (this.state.redirect === true) {
            return <Redirect to="/add-product" push={true} />
        }
        else {
            return (
                <div>
                    {/* Header */}
                    <Row>
                        <Col xs={9} sm={9} md={9} lg={9}>
                            <h4>
                                {props.name}
                            </h4>
                        </Col>
                        <Col xs={3} sm={3} md={3} lg={3} style={{ textAlign: 'right' }}>
                            {
                                props.name === 'Products'
                                    ? <Button onClick={this._redirect}>
                                        +
                                </Button>

                                    : this.state.display === 'none'
                                        ? <Button onClick={() => this.setState({ display: 'block' })}>
                                            +
                                      </Button>
                                        : <Button onClick={() => this.setState({ display: 'none' })}>
                                            x
                                      </Button>
                            }
                        </Col>
                    </Row>

                    {/* Form */}
                    <Well style={{ display: this.state.display }}>
                        <Row>
                            <Col md={col} lg={col}>
                                <FormControl
                                    onChange={this._onChange}
                                    placeholder="Name"
                                    type="text" name='name' value={this.state.name} />
                            </Col>
                            {col === 5 ?
                                <Col md={colForSelect} lg={colForSelect}>
                                    <FormControl
                                        onChange={this._onChange}
                                        value={this.state.country}
                                        name='country'
                                        componentClass="select">
                                        <option value="Select"> - - - - - </option>
                                        <option value="India">India</option>
                                        <option value="Other">Other</option>
                                    </FormControl>
                                </Col> : null}
                            {
                                col === 3 && (
                                    <React.Fragment>
                                        <Col md={col} lg={col}>

                                            <FormControl
                                                onChange={this._onChange}
                                                placeholder="Contact"
                                                type="text" name='contact' value={this.state.contact} />
                                        </Col>

                                        <Col md={col + 1} lg={col + 1}>
                                            <FormControl
                                                onChange={this._onChange}
                                                placeholder="Email"
                                                type="text" name='email' value={this.state.email} />
                                        </Col>
                                    </React.Fragment>
                                )
                            }
                            <Col xs={2} sm={2} md={2} lg={2} >
                                <Button
                                    onClick={this._onClick}>
                                    Save
                            </Button>
                            </Col>
                        </Row>
                    </Well>
                </div>
            )
        }
    }
}
export default TableHeader