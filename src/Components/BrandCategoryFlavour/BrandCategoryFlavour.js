import React from 'react'
import { Row, Grid } from 'react-bootstrap'
import BrandCategoryFlavourTable from './BrandCategoryFlavourTable'
import NavBar from '../NavBar'
import { _tr } from '../CommonFunctions/TableRow'

import { connect } from 'react-redux'
import {
    getBrands,
    getCategories,
    getFlavours,
    onChange,
    addNew,
} from '../../Redux/Actions/BrandCategoryFlavourAction'

class BrandCategoryFlavour extends React.Component {

    componentDidMount = () => {
        const {cookies} = this.props
        const cookie = cookies.get('token')
        this.props.getBrands(cookie)
        this.props.getCategories(cookie)
        this.props.getFlavours(cookie)
    }


    _generateList = (data, type) => {
        const {cookies} = this.props
        const cookie = cookies.get('token')
        let collections = data.map(item => (
            _tr(item, type, this.props.onChange , cookie)
        ))

        return collections
    }

    render() {
        const { cookies } = this.props
            return (
                <React.Fragment>
                    <NavBar cookies={cookies}/>
                    <Grid fluid>
                        <Row style={{ margin: 10 }}>
                            <BrandCategoryFlavourTable
                                lg='4'
                                offset='1'
                                renderTable={this._generateList}
                                data={this.props.state.brands}
                                type='Brands'
                                addNew={this.props.addNew}
                                cookies={cookies}
                            />
                            <BrandCategoryFlavourTable
                                lg='3'
                                offset='0'
                                renderTable={this._generateList}
                                data={this.props.state.categories}
                                type='Categories'
                                addNew={this.props.addNew}
                                cookies={cookies}
                            />
                            <BrandCategoryFlavourTable
                                offset='0'
                                lg='3'
                                renderTable={this._generateList}
                                data={this.props.state.flavours}
                                type='Flavours'
                                addNew={this.props.addNew}
                                cookies={cookies}
                            />

                        </Row>
                    </Grid>
                </React.Fragment>
            )
    }
}


const mapStateToProps = (state, ownProps) => {
    return ({
        state: state,
        cookies: ownProps.cookies,
    });
};

const mapDispatchToProps = {
    getBrands,
    getCategories,
    getFlavours,
    onChange,
    addNew
}

const BKFCConnect = connect(
    mapStateToProps,
    mapDispatchToProps
)(BrandCategoryFlavour)

export default BKFCConnect