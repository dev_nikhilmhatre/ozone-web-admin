import React from 'react'
import { Col, Panel, Table } from 'react-bootstrap'

import TableTitle from '../CommonFunctions/TableTitle'

const BrandCategoryFlavourTable = (props) => {
    return (
        <Col
            xsOffset={props.type === 'Brands' ? window.innerWidth >= 1024 ? 1 : 0 : 0}
            lg={props.type === 'Brands' ? 4 : 3}>
            <TableTitle
                name={props.type}
                addNew={props.addNew}
                cookies={props.cookies} />
            <hr />
            <Panel>
                <Table responsive>
                    <thead>
                        <tr>
                            <th style={{ paddingLeft: 18 }}>
                                Name
                            </th>
                            {
                                props.type === 'Brands' ?
                                    <th>
                                        Country
                                    </th>
                                    : null
                            }
                            <th style={{ textAlign: 'center' }}>
                                Disabled
                            </th>
                        </tr>

                    </thead>
                    <tbody>
                        {
                            props.data !== undefined
                                ? props.renderTable(props.data, props.type)
                                : null
                        }
                    </tbody>
                </Table>
            </Panel>
        </Col>
    )
}

export default BrandCategoryFlavourTable