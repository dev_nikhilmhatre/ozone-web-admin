import React from 'react'
import NavBar from '../NavBar'
import { connect } from 'react-redux'

class Dashboard extends React.Component {


    render() {
        const { cookies } = this.props
            return (
                <div>
                    <NavBar cookies={cookies} />
                    <div style={{ width: 150, margin: '150px auto' }}>
                        <h1>
                            Dashboard
                        </h1>
                    </div>
                </div>
            )
    }
}

const mapStateToProps = (state, ownProps) => {
    return ({
        state: state,
        cookies: ownProps.cookies,
    });
};

const DashboardConnect = connect(
    mapStateToProps,
    null
)(Dashboard)

export default DashboardConnect