import React from 'react'
import { Col, Panel, Table, FormControl, Button } from 'react-bootstrap'
import TableTitle from '../CommonFunctions/TableTitle'



const CustomeTable = (props) => (
    <Col lg={8}>
        <TableTitle
            name='Clients'
            addNew={props.addNew}
            cookies={props.cookies}
        />
        <hr />
        <Panel>
            <Table responsive>
                <thead>
                    <tr>
                        {
                            props.headers.map(item => <th key={item}>{item}</th>)
                        }
                    </tr>
                </thead>
                <tbody>
                    {
                        Object.keys(props.data).map(item => (
                            <tr key={props.data[item].id}>
                                <td>
                                    <FormControl
                                        obj={props.data[item].id}
                                        style={{ border: 'none', boxShadow: 'none', background: 'white' }}
                                        onChange={props._clientOnChange}
                                        placeholder="Name"
                                        type="text" name='name' value={props.data[item].name} />

                                </td>
                                <td>
                                    <FormControl
                                        obj={props.data[item].id}
                                        style={{ border: 'none', boxShadow: 'none', background: 'white' }}
                                        onChange={props._clientOnChange}
                                        placeholder="Contact"
                                        type="text" name='contact' value={props.data[item].contact} />
                                </td>
                                <td>

                                    <FormControl
                                        obj={props.data[item].id}
                                        style={{ border: 'none', boxShadow: 'none', background: 'white' }}
                                        onChange={props._clientOnChange}
                                        placeholder="Email"
                                        type="text" name='email' value={props.data[item].email} />
                                </td>
                                <td>
                                    <Button
                                        obj={props.data[item].id}
                                        onClick={props._clientUpdate}
                                    >
                                        Update
                                    </Button>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
            </Table>
        </Panel>
    </Col>
)
export default CustomeTable
