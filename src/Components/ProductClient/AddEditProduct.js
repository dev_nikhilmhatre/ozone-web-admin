import React, { Component, Fragment } from 'react';
import {
    Col,
    ControlLabel,
    FormControl,
    FormGroup,
    Grid,
    Row,
    Nav,
    NavItem,
    Well,
    Button,
    Glyphicon,
    Modal
} from 'react-bootstrap';

import NavBar from '../NavBar'
import { base_url } from '../../Common/Constants'
import { getData, addNew, uploadFile, updateData } from '../../Common/apiCalls'
import { toast } from 'react-toastify'
import { Redirect } from 'react-router-dom'


// ---------------------------------------------------------------------------------
function PrimaryDetails(props) {

    let brands = props.brands.map(brand => <option key={brand.id} value={brand.id}>{brand.name}</option>)
    let categories = props.categories.map(category => <option key={category.id} value={category.id}>{category.name}</option>)

    return (
        <Row id="primary-detail">
            <Col xs={12} sm={6} md={3} lg={3} >
                <FormGroup>
                    <ControlLabel>Product Name</ControlLabel>
                    <FormControl value={props.state.productName} name="productName" type="text" onChange={props.onChange} />
                </FormGroup>
            </Col>

            <Col xs={12} sm={6} md={3} lg={3} >
                <FormGroup>
                    <ControlLabel>Brand</ControlLabel>
                    <FormControl value={props.state.brand} name="brand" componentClass="select" onChange={props.onChange}>
                        <option value='Select'>Select</option>
                        {brands}
                    </FormControl>
                </FormGroup>
            </Col>

            <Col xs={12} sm={6} md={3} lg={3} >
                <FormGroup>
                    <ControlLabel>Category</ControlLabel>
                    <FormControl value={props.state.category} name="category" componentClass="select" onChange={props.onChange} >
                        <option value='Select'>Select</option>
                        {categories}
                    </FormControl>
                </FormGroup>
            </Col>

            <Col xs={12} sm={6} md={3} lg={3} >
                <FormGroup>
                    <ControlLabel>Non-Veg Product...?</ControlLabel>
                    <FormControl value={props.state.nonVeg} name="nonVeg" componentClass="select" onChange={props.onChange}>
                        <option value="">Select</option>
                        <option value={true}>Yes</option>
                        <option value={false}>No</option>
                    </FormControl>
                </FormGroup>
            </Col>

            <Col xs={12} sm={6} md={6} lg={6} >
                <FormGroup>
                    <ControlLabel>Key Features</ControlLabel>
                    <FormControl style={{ height: 125 }} name="key_features" componentClass="textarea" value={props.state.key_features} onChange={props.onChange} />
                </FormGroup>
            </Col>

            <Col xs={12} sm={6} md={6} lg={6} >
                <FormGroup>
                    <ControlLabel>Description</ControlLabel>
                    <FormControl name="description" value={props.state.description} componentClass="textarea" style={{ height: 125 }} onChange={props.onChange} />
                </FormGroup>
            </Col>
        </Row>
    )
};

// ---------------------------------------------------------------------------------

function NavTabs(props) {
    return (
        <Fragment>
            <div style={{ width: "100%", textAlign: "right" }}>
                <Button
                    obj={props.wellId}
                    onClick={props.onWeightDelete}
                    disabled={props.len < 2 ? true : false}>
                    <Glyphicon obj={props.wellId} glyph="glyphicon glyphicon-trash" />
                </Button>
            </div>

            <Nav bsStyle="tabs" activeKey={props.activeTab}>
                <NavItem id={props.wellId + 'x'} eventKey={props.wellId + 'x'} onClick={props.onChangeTab}>
                    Weight
            </NavItem>
                <NavItem id={props.wellId + 'y'} eventKey={props.wellId + 'y'} onClick={props.onChangeTab}>
                    Flavour
            </NavItem>
            </Nav>
        </Fragment>
    )
};

// ---------------------------------------------------------------------------------

function WeightDetail(props) {

    const wd = props.weightDetail
    const wellId = props.wellId
    const onChange = props.onChange

    return (
        <Row key={wellId} style={props.activeTab[props.wellId] === props.wellId + 'x' ? {} : { 'display': 'none' }}>
            <Col xs={12} sm={6} md={3} lg={3}  >
                <FormGroup>
                    <ControlLabel>Purchase Price</ControlLabel>
                    <FormControl obj={wellId} onChange={onChange} name="purchasePrice" type="text" value={wd.purchasePrice} />
                </FormGroup>
            </Col>

            <Col xs={12} sm={6} md={3} lg={3} >
                <FormGroup>
                    <ControlLabel>MRP</ControlLabel>
                    <FormControl type="text" obj={wellId} onChange={onChange} name="mrp" value={wd.mrp} />
                </FormGroup>
            </Col>

            <Col xs={12} sm={6} md={3} lg={3} >
                <FormGroup>
                    <ControlLabel>Discount</ControlLabel>
                    <FormControl type="text" obj={wellId} onChange={onChange} name="discount" value={wd.discount} />
                </FormGroup>
            </Col>


            <Col xs={12} sm={6} md={3} lg={3} >
                <FormGroup>
                    <ControlLabel>Weight</ControlLabel>
                    <FormControl type="text" obj={wellId} onChange={onChange} name="weight" value={wd.weight} />
                </FormGroup>
            </Col>
        </Row>
    )
};

// ---------------------------------------------------------------------------------

class FlvDetail extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            urls: [],
            show: false,
            prntobj: '',
            obj: '',
            disabled: false
        }
    }

    openPopUp = (e) => {
        let fds = this.props.flvDetails[e.target.getAttribute('obj')]
        let urls = [...this.state.urls]
        fds.images.forEach(oldImg => {
            oldImg = oldImg.id !== undefined ? oldImg.image : oldImg
            urls.push({
                oldimg: true,
                image: oldImg,
                name: oldImg,
                uploading: 'done',
            })
        })
        this.setState({ show: true, urls, disabled: true })
    }


    openDialogue = () => {
        this.uploadImage.click()
    }

    onChange = (e) => {
        this.setState({ disabled: false })
        const target = e.target

        this.setState({
            prntobj: target.getAttribute('prntobj'),
            obj: target.getAttribute('obj')
        })

        const files = target.files

        if (files.length > 0) {
            let urls = [...this.state.urls]

            Object.keys(files).forEach(i => {
                let image = URL.createObjectURL(files[i])
                urls.push({
                    image,
                    name: files[i].name,
                    file: files[i],
                    uploading: 'no', // no, progress, done
                })
            })

            this.setState({ urls })
        }
    }

    upload = () => {

        this.setState({ disabled: true })

        let total = this.state.urls.length
        let count = 0
        let uploadedData = []
        this.state.urls.forEach(item => {

            if (item.uploading !== 'done') {

                this.setState((state) => {
                    return state.urls.map(i => {
                        if (item.name === i.name) {
                            i.uploading = 'progress'
                            return i
                        } else {
                            return i
                        }
                    })
                })

                let formData = new FormData()
                formData.append('image', item.file)

                let url = `${base_url}/api-product/media-post/`
                const token = this.props.cookies.get('token')

                uploadFile(url, token, formData)
                    .then(data => {
                        this.setState((state) => {
                            return state.urls.map(i => {
                                if (item.name === i.name) {
                                    i.uploading = 'done'
                                    return i
                                } else {
                                    return i
                                }
                            })
                        })
                        count++
                        uploadedData.push(data)
                        if (total === count) {
                            this.props.afterImageUpload(this.state.obj, this.state.prntobj, uploadedData)
                        }

                    })
                    .catch(err => console.log(err))
            } else {
                count++
            }
        })
    }

    onCancelClick = (e) => {
        let tempArray = []
        this.state.urls.forEach(item => {
            if (item.name !== e.target.getAttribute('data-name')) {
                tempArray.push(item)
            }
        })

        this.setState({ urls: tempArray })
        if (tempArray.length === 0) {
            this.setState({ show: false })
        }
    }

    handleHide = () => {
        this.setState({ urls: [], show: false })
    }

    imageList = (data) => {
        return data.map(item => (
            <Col xs={6} sm={3} md={3} lg={3} key={item.name} style={{ padding: 5, position: 'relative' }}>
                <div
                    style={{
                        zIndex: '2',
                        position: 'absolute',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        opacity: 0.5,
                        height: 130,
                        width: 130,
                        // display: item.uploading === 'no' ? 'none' : 'block'
                    }}>
                    <h4 style={{ textAlign: "center", color: 'white' }}>
                        {item.uploading === 'no'
                            ? <span style={{ fontSize: 20, cursor: 'pointer' }} data-name={item.name} onClick={this.onCancelClick}>
                                Cancel
                            </span>
                            : item.uploading === 'done'
                                ? <span style={{ fontSize: 20 }}>Complete</span>
                                : <span style={{ fontSize: 20 }}>Uploading...</span>
                        }
                    </h4>
                </div>
                <img src={item.oldimg !== undefined ? base_url + item.image : item.image} alt="" style={{ width: '130px', height: '130px', padding: 5 }} />
            </Col>
        ))
    }

    render() {

        let list = []
        const { props } = this
        const fd = props.flvDetails
        let len = Object.keys(fd).length

        let flavours = props.flavours.map(flavour => <option key={flavour.id} value={flavour.id} >{flavour.name}</option>)

        for (let id in fd) {

            let item = (
                <Row key={id}>
                    <Col xs={12} sm={6} md={3} lg={3}  >
                        <FormGroup>
                            <ControlLabel>Flavour</ControlLabel>
                            <FormControl prntobj={props.wellId} obj={id} componentClass="select" name="flavour" value={fd[id].flavour} onChange={props.onChangeFlv} >
                                <option value='Select'>Select</option>
                                {flavours}
                            </FormControl>
                        </FormGroup>
                    </Col>

                    <Col xs={12} sm={6} md={3} lg={3} >
                        <FormGroup>
                            <ControlLabel>Quantity</ControlLabel>
                            <FormControl prntobj={props.wellId} obj={id} type="text" name="quantity" value={fd[id].quantity} onChange={props.onChangeFlv} />
                        </FormGroup>
                    </Col>

                    <Col xs={12} sm={6} md={3} lg={3} >
                        <FormGroup>
                            <ControlLabel>Images</ControlLabel>
                            <div>
                                <Button obj={id} onClick={this.openPopUp} block>Choose Files</Button>
                            </div>
                            <FormControl
                                style={{ display: 'none' }}
                                inputRef={ref => this.uploadImage = ref}
                                prntobj={props.wellId} obj={id} name="image" type="file" multiple onChange={this.onChange} />
                        </FormGroup>
                    </Col>

                    <Col xs={12} sm={6} md={3} lg={3} >
                        <br />
                        <Button disabled={len < 2 ? true : false}
                            onClick={props.onFlvDelete}
                            prntobj={props.wellId}
                            obj={id} block>
                            Delete
                    </Button>
                    </Col>
                </Row>
            )

            list = [...list, item]
        };

        return (
            <div
                style={props.activeTab[props.wellId] === props.wellId + 'x' ? { 'display': 'none' } : {}}>
                {list}
                <hr />

                {/* Modal start */}
                <Modal
                    show={this.state.show}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title">
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">
                            Files
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            {this.imageList(this.state.urls)}
                            <Col xs={6} sm={3} md={3} lg={3} style={{ padding: 5 }}>
                                <Button onClick={this.openDialogue} style={{ fontSize: '25px', width: '130px', height: '130px' }}>
                                    +
                                </Button>
                            </Col>
                        </Row>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button disabled={this.state.disabled} onClick={this.upload}>Upload</Button>
                    </Modal.Footer>
                </Modal>
                {/* Modal end */}

                <div style={{ textAlign: "right" }}>
                    <Button
                        id={props.wellId}
                        onClick={props.addNewFlavour}>
                        Add Flavour
                </Button>
                </div>
            </div>
        )
    }
};

// ---------------------------------------------------------------------------------

class AddEditProduct extends Component {

    _isMounted = false

    constructor(props) {
        super(props)

        this.state = {
            disabled: false,
            res: undefined,

            brandList: [],
            flavourList: [],
            categoryList: [],
            keyFeatureList: [],

            activeTab: { 1: '1x' },

            productName: '',
            brand: 0,
            category: 0,
            nonVeg: 0,
            key_features: '',
            description: '',
            productId: '',

            weightDetail: {
                1: {
                    purchasePrice: '',
                    mrp: '',
                    discount: '',
                    weight: '',
                    flvDetails: {
                        1: {
                            flavour: 0,
                            quantity: 0,
                            images: [],
                        }
                    }
                }
            }
        };
    }



    componentDidMount = () => {
        this._isMounted = true

        getData(`${base_url}/api-brand/list-c/`, '')
            .then(res => {
                if (this._isMounted) {
                    this.setState({ brandList: res })
                }
            })


        getData(`${base_url}/api-category/list-c/`, '')
            .then(res => {
                if (this._isMounted) {
                    this.setState({ categoryList: res })
                }
            })


        getData(`${base_url}/api-flavour/list-c/`, '')
            .then(res => {
                if (this._isMounted) {
                    this.setState({ flavourList: res })
                }
            })

        // getData(`${base_url}/api-product/key-fetures-list-a/`, '')
        //     .then(res => {
        //         if (this._isMounted) {
        //             this.setState({ keyFeatureList: res })
        //         }
        //     })

        if (this.props.match !== undefined) {
            getData(`${base_url}/api-product/product-retrive-a/${this.props.match.params.id}/`, '')
                .then(res => {

                    let weightDetail = {}
                    let activeTab = {}
                    res.productinfomodel_set.forEach((p, id1) => {
                        id1 = id1 + 1
                        activeTab[id1] = `${id1}x`
                        let flvDetails = {}
                        p.quantitymodel_set.forEach((f, id2) => {
                            id2 = id2 + 1
                            flvDetails[id2] = {
                                id: f.id,
                                product_info: f.product_info,
                                flavour: f.flavours,
                                images: f.images,
                                quantity: f.quantity
                            }
                        })
                        weightDetail[id1] = {
                            product: p.product,
                            id: p.id,
                            purchasePrice: p.purchase_price,
                            weight: p.weight_lbs,
                            mrp: p.mrp,
                            discount: p.discount,
                            flvDetails
                        }
                    })

                    if (this._isMounted) {
                        this.setState({
                            productId: res.id,
                            productName: res.name,
                            brand: res.brand,
                            category: res.category,
                            key_features: res.key_features,
                            description: res.description,
                            nonVeg: res.is_non_veg,
                            weightDetail,
                            activeTab
                        })
                    }
                })
        }

    }

    componentWillUnmount = () => {
        this._isMounted = false
    }


    onChange = (e) => {
        let name = e.target.name
        this.setState({ [name]: e.target.value })
    }


    onChangeWeight = (e) => {
        let target = e.target
        let objId = target.getAttribute('obj')
        let weightDetail = this.state.weightDetail
        let currentObject = weightDetail[objId]

        currentObject[target.name] = target.value
        weightDetail[objId] = currentObject

        this.setState({ weightDetail: Object.assign({}, weightDetail) })
    }

    onWeightDelete = (e) => {
        let target = e.target
        let objId = target.getAttribute('obj')
        let weightDetail = this.state.weightDetail

        delete weightDetail[objId]

        this.setState({ weightDetail: Object.assign({}, weightDetail) })
    }

    afterImageUpload = (objId, prntobjId, data) => {

        let weightDetail = this.state.weightDetail
        let prntObject = weightDetail[prntobjId]
        let currentObject = prntObject.flvDetails[objId]

        currentObject['images'] = [...currentObject['images'], ...data]

        prntObject.flvDetails[objId] = currentObject
        weightDetail[prntobjId] = prntObject

        this.setState({ weightDetail: Object.assign({}, weightDetail) })
    }

    onChangeFlv = (e) => {

        let target = e.target
        let objId = target.getAttribute('obj')
        let prntobjId = target.getAttribute('prntobj')
        let weightDetail = this.state.weightDetail
        let prntObject = weightDetail[prntobjId]
        let currentObject = prntObject.flvDetails[objId]

        currentObject[target.name] = target.value

        prntObject.flvDetails[objId] = currentObject
        weightDetail[prntobjId] = prntObject

        this.setState({ weightDetail: Object.assign({}, weightDetail) })
    }

    onFlvDelete = (e) => {
        let target = e.target
        let objId = target.getAttribute('obj')
        let prntobjId = target.getAttribute('prntobj')
        let weightDetail = this.state.weightDetail
        let prntObject = weightDetail[prntobjId]

        delete prntObject.flvDetails[objId]

        weightDetail[prntobjId] = prntObject

        this.setState({ weightDetail: Object.assign({}, weightDetail) })
    }

    onChangeTab = (e) => {
        let id = e.target.id.substr(0, e.target.id.length - 1)
        let tempState = this.state.activeTab
        tempState[id] = e.target.id

        this.setState({ activeTab: Object.assign({}, tempState) })
    }

    addNewWeight = () => {
        let tempState = []
        let weightDetail = {
            purchasePrice: '',
            mrp: '',
            discount: '',
            weight: '',
            flvDetails: {
                1: {
                    flavour: 0,
                    quantity: 0,
                    images: [],
                }
            }
        }

        for (let a in this.state.weightDetail) {
            tempState.push(parseInt(a, 10))
        }

        let key = Math.max(...tempState) + 1
        let currentState = this.state.weightDetail
        currentState[key] = weightDetail

        this.setState({ weightDetail: Object.assign({}, currentState) })

        let activeTab = this.state.activeTab;
        activeTab[key] = key + 'x';

        this.setState({ activeTab: Object.assign({}, activeTab) })
    };

    addNewFlavour = (e) => {
        let tempState = []
        let flvDetail = {
            flavour: 0,
            quantity: 0,
            images: [],
        }

        let id = e.target.id
        for (let a in this.state.weightDetail[id].flvDetails) {
            tempState.push(parseInt(a, 10))
        }

        let key = Math.max(...tempState) + 1
        let currentState = this.state.weightDetail[id]
        currentState.flvDetails[key] = flvDetail


        let weightDetail = this.state.weightDetail
        weightDetail[id] = currentState

        this.setState({ weightDetail: Object.assign({}, weightDetail) })
    }

    otherDetailList = (weightDetail, addNewFlavour, flavours) => {
        let list = []
        const { cookies } = this.props
        for (let id in weightDetail) {
            let item = (
                <Well key={id}>
                    <NavTabs
                        len={Object.keys(this.state.weightDetail).length}
                        wellId={id}
                        activeTab={this.state.activeTab[id]}
                        onChangeTab={this.onChangeTab}
                        onWeightDelete={this.onWeightDelete} />
                    <br />
                    <WeightDetail
                        wellId={id}
                        activeTab={this.state.activeTab}
                        weightDetail={weightDetail[id]}
                        onChange={this.onChangeWeight} />
                    <FlvDetail
                        flavours={flavours}
                        addNewFlavour={addNewFlavour}
                        wellId={id}
                        flvDetails={weightDetail[id].flvDetails}
                        activeTab={this.state.activeTab}
                        onChangeFlv={this.onChangeFlv}
                        onFlvDelete={this.onFlvDelete}
                        afterImageUpload={this.afterImageUpload}
                        cookies={cookies} />
                </Well>
            )
            list = [...list, item]
        };

        return list;
    };




    onSubmit = () => {
        this.setState({ disabled: true })
        let url = ''
        let state = { ...this.state }
        const token = this.props.cookies.get('token')

        delete state['brandList']
        delete state['flavourList']
        delete state['categoryList']
        delete state['keyFeatureList']
        delete state['activeTab']
        delete state['res']
        delete state['disabled']

        let allow = true

        if (state.brand === 0
            && state.category === 0
            && state.description.length === 0
            && state.key_features.length === 0
            && state.nonVeg === 0
            && state.productName.length === 0) {
            allow = false
        }

        if (allow) {
            if (Object.keys(state.weightDetail).length === 0) {
                allow = false
            } else {
                Object.keys(state.weightDetail).forEach(wtKey => {
                    let wt = state.weightDetail[wtKey]
                    if (wt.discount.length === 0
                        && wt.purchasePrice.length === 0
                        && wt.weight.length === 0) {
                        allow = false
                    }
                    if (allow) {
                        if (Object.keys(wt.flvDetails).length === 0) {
                            allow = false
                        } else {
                            Object.keys(wt.flvDetails).forEach(flv => {
                                if (wt.flvDetails[flv].flavour === 0
                                    && wt.flvDetails[flv].images.length === 0) {
                                    allow = false
                                }
                            })
                        }

                    }
                })
            }

        }


        if (allow) {
            if (this.props.match === undefined) {
                url = `${base_url}/api-product/product-create-a/`
                addNew(url, state, token).then(res => {
                    this.setState({ disabled: false })
                    if (res.Error) {
                        toast.warn('Please check if all the fields are filled properly.', {
                            position: toast.POSITION.BOTTOM_RIGHT,
                            hideProgressBar: true
                        })
                    } else {
                        toast.info('Product added Successfully', {
                            position: toast.POSITION.BOTTOM_RIGHT,
                            hideProgressBar: true
                        })
                        this.setState({ res: res.id })
                    }

                }).catch(err => {
                    this.setState({ disabled: false })
                    toast.error(`Something went wrong, Please try again later.`, {
                        position: toast.POSITION.BOTTOM_RIGHT,
                        hideProgressBar: true
                    })
                })
            } else {
                url = `${base_url}/api-product/product-update-a/${this.state.productId}/`
                updateData(url, state, token).then(res => {
                    this.setState({ disabled: false })
                    if (res.Error) {
                        toast.warn('Please check if all the fields are filled properly.', {
                            position: toast.POSITION.BOTTOM_RIGHT,
                            hideProgressBar: true
                        })
                    } else {
                        toast.info('Product updated Successfully', {
                            position: toast.POSITION.BOTTOM_RIGHT,
                            hideProgressBar: true
                        })
                        this.setState({ res: res.id })
                    }

                }).catch(err => {
                    this.setState({ disabled: false })
                    toast.error(`Something went wrong, Please try again later.`, {
                        position: toast.POSITION.BOTTOM_RIGHT,
                        hideProgressBar: true
                    })
                })
            }
        } else {
            this.setState({ disabled: false })
            toast.warn('Please check if all the fields are filled properly.', {
                position: toast.POSITION.BOTTOM_RIGHT,
                hideProgressBar: true

            })
        }
    }

    render() {
        const { cookies } = this.props

        if (this.state.res !== undefined) {
            return <Redirect to={`/product-client`} />
        }

        return (
            <React.Fragment>
                <NavBar cookies={cookies} />
                <Grid>
                    {
                        this.props.match === undefined ?
                            <h2>Add Product</h2> :
                            <h2>Update Product</h2>
                    }

                    <Well>
                        <PrimaryDetails brands={this.state.brandList}
                            categories={this.state.categoryList}
                            keyFetures={this.state.keyFeatureList}
                            state={this.state} onChange={this.onChange} />
                    </Well>
                    <div style={{ margin: 10, textAlign: 'right' }}>
                        <Button onClick={this.addNewWeight}>
                            Add Weight Tab
                        </Button>
                    </div>
                    {
                        this.otherDetailList(this.state.weightDetail, this.addNewFlavour, this.state.flavourList)
                    }
                    <br />
                    <div style={{ width: "25%", margin: "0 auto 35px auto" }}>
                        <Button
                            disabled={this.state.disabled}
                            onClick={this.onSubmit}
                            block>
                            Submit
                    </Button>
                    </div>
                </Grid>
            </React.Fragment>
        )
    };
};

export default AddEditProduct;