import React from 'react'
import NavBar from '../NavBar'
import ProductTable from './ProductTable'
import CustomTable from './CustomeTable'
import { Grid, Row } from 'react-bootstrap'
import { _tr } from '../CommonFunctions/TableRow'
import { Link } from 'react-router-dom'
import { addNew } from '../../Redux/Actions/BrandCategoryFlavourAction'
import { connect } from 'react-redux'
import { toast } from 'react-toastify';
import {
    getProducts,
    getClients,
    onClientChange
} from '../../Redux/Actions/ProductClientAction'
import { updateData } from '../../Common/apiCalls'
import { base_url } from '../../Common/Constants'
// import {
//     onChange
// } from '../../Redux/Actions/BrandCategoryFlavourAction'

class Product extends React.Component {


    componentDidMount = () => {
        const { cookies } = this.props
        const cookie = cookies.get('token')
        this.props.getProducts(cookie)
        this.props.getClients(cookie)
    }

    _clientOnChange = (e) => {
        this.props.onClientChange({ id: e.target.getAttribute('obj'), [e.target.name]: e.target.value })
    }

    _clientUpdate = (e) => {
        const { cookies } = this.props
        const cookie = cookies.get('token')
        let id = e.target.getAttribute('obj')
        updateData(
            `${base_url}/api-client/retrive-update-destroy-a/${id}/`,
            this.props.state.clients[id],
            cookie
        ).then(res => {
            toast.info(`Client updated successfully`, {
                position: toast.POSITION.BOTTOM_RIGHT,
                hideProgressBar: true
            })
            this.props.onClientChange(res)
        }).catch(err => {
            toast.error(`Something went wrong, Please try again later.`, {
                position: toast.POSITION.BOTTOM_RIGHT,
                hideProgressBar: true
            })
        })
    }

    _generateList = (data) => {

        return data.map(item => (
            <tr key={item.id}>
                <td>
                    {item.name}
                </td>
                <td>
                    <Link
                        className="btn btn-default"
                        to={`/edit-product/${item.id}`} style={{ width: '100%' }}>
                        Edit
                    </Link>
                </td>
            </tr>
        ))
    }

    _generateList2 = (data, type) => {
        const { cookies } = this.props
        const cookie = cookies.get('token')
        let collections = data.map(item => (
            _tr(item, type, this.props.onChange, cookie)
        ))

        return collections
    }

    render() {
        const { cookies } = this.props
        return (
            <React.Fragment>
                <NavBar cookies={cookies} />
                <Grid>
                    <Row style={{ margin: 10 }}>
                        <ProductTable
                            renderTable={this._generateList}
                            data={this.props.state.products}
                        />

                        <CustomTable
                            headers={['Name', 'Contact', 'Email', 'Action']}
                            data={this.props.state.clients}
                            addNew={this.props.addNew}
                            cookies={cookies}
                            _clientOnChange={this._clientOnChange}
                            _clientUpdate={this._clientUpdate}
                        />
                    </Row>
                </Grid>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return ({
        state: state,
        cookies: ownProps.cookies,
    });
};

const mapDispatchToProps = {
    getProducts,

    getClients,
    addNew,
    onClientChange
}

const ProductConnect = connect(
    mapStateToProps,
    mapDispatchToProps
)(Product)

export default ProductConnect