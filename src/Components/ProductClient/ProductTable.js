import React from 'react'
import { Col, Panel, Table } from 'react-bootstrap'
import TableTitle from '../CommonFunctions/TableTitle'

const ProductTable = (props) => (
    
    <Col lg={4}>
        <TableTitle
            name='Products' />
        <hr />
        <Panel>
            <Table responsive>
                <thead>
                    <tr>
                        <th>
                            Name
                        </th>
                       
                        <th style={{ textAlign: 'center' }}>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        props.data !== undefined
                            ? props.renderTable(props.data)
                            : null
                    }
                </tbody>
            </Table>
        </Panel>
    </Col>
)

export default ProductTable