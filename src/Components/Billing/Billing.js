import React from 'react'
import NavBar from '../NavBar'
import { connect } from 'react-redux'

const Billing = (props) => (
    <div>
        <NavBar cookies={props.cookies} />
        <div style={{ width: 150, margin: '150px auto' }}>
            <h1>
                Billing
            </h1>
        </div>
    </div>
)

const mapStateToProps = (state, ownProps) => {
    return ({
        state: state,
        cookies: ownProps.cookies,
    });
};

const BillingConnect = connect(
    mapStateToProps,
    null
)(Billing)


export default BillingConnect