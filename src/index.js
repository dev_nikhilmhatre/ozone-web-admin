import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import App from './App';
// import * as serviceWorker from './serviceWorker';

import store from './Redux/Store'
import { Provider } from 'react-redux'

import { CookiesProvider } from 'react-cookie'

ReactDOM.render(
    <CookiesProvider>
        <Provider store={store}>
            <App />
        </Provider>
    </CookiesProvider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
