export const actions = {
    getBrands: 'getBrands',
    getCategories: 'getCategories',
    getFlavours: 'getFlavours',
    getProducts: 'getProducts',
    // getKeyFetures: 'getKeyFetures',

    onChangeBrand: 'onChangeBrand',
    onChangeCategory: 'onChangeCategory',
    onChangeFlavour: 'onChangeFlavour',
    // onChangeKeyFetures: 'onChangeKeyFetures',

    newBrand: 'newBrand',
    newCategory: 'newCategory',
    newFlavour: 'newFlavour',
    // newKeyFetures: 'newKeyFetures',

    getClients:'getClients',
    newClient:'newClient',
    onChangeClient:'onChangeClient'

}