import { combineReducers } from 'redux'

import { brands, categories, flavours } from './Reducers/BrandCategoryFlavourReducer'
import { products, clients } from './Reducers/ProductClientReducer'

const rootReducer = combineReducers({
    brands,
    categories,
    flavours,
    products, 
    clients
})

export default rootReducer