import { actions } from '../AllActions'

export const brands = (state = [], action) => {
    switch (action.type) {
        case actions.getBrands:
            return [...action.payload]
        case actions.onChangeBrand:
            return updateData(state, action)
        case actions.newBrand:
            return [action.payload, ...state]
        default:
            return state
    }
}

export const categories = (state = [], action) => {
    switch (action.type) {
        case actions.getCategories:
            return [...action.payload]
        case actions.onChangeCategory:
            return updateData(state, action)
        case actions.newCategory:
            return [action.payload, ...state]
        default:
            return state
    }
}

export const flavours = (state = [], action) => {
    switch (action.type) {
        case actions.getFlavours:
            return [...action.payload]
        case actions.onChangeFlavour:
            return updateData(state, action)
        case actions.newFlavour:
            return [action.payload, ...state]
        default:
            return state
    }
}

// export const keyFetures = (state = [], action) => {
//     switch (action.type) {
//         case actions.getKeyFetures:
//             return [...action.payload]
//         case actions.onChangeKeyFetures:
//             return updateData(state, action)
//         case actions.newKeyFetures:
//             return [action.payload, ...state]
//         default:
//             return state
//     }
// }

const updateData = (state, action) => {
    return state.map(item =>
        (item.id === action.payload.id)
            ? action.payload.inputType === 'text'
                ? { ...item, name: action.payload.value }
                : action.payload.inputType === 'checkbox'
                    ? { ...item, is_disabled: action.payload.value }
                    : { ...item, country: action.payload.value }
            : item
    )
}
