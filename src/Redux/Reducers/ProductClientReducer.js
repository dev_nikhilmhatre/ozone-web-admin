import { actions } from '../AllActions'

export const products = (state = [], action) => {
    switch (action.type) {
        case actions.getProducts:
            return [...action.payload]
        default:
            return state
    }
}

export const clients = (state = [], action) => {
    switch (action.type) {
        case actions.getClients:
            return { ...action.payload }
        case actions.newClient:
            return { [action.payload.id]: action.payload, ...state }
        case actions.onChangeClient:
            return Object.assign({}, state, {
                [action.payload.id]: { ...state[action.payload.id], ...action.payload }
            })
        default:
            return state
    }
}