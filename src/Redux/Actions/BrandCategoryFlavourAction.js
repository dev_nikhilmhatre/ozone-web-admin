import { actions } from '../AllActions'
import { base_url } from '../../Common/Constants'
import { getData } from '../../Common/apiCalls'

export const getBrands = (token) => {
    return dispatch => {
        getData(`${base_url}/api-brand/list-c/`, token).then(res => {
            dispatch({
                type: actions.getBrands,
                payload: res
            })
        })
    }
}

export const getCategories = (token) => {
    return dispatch => {
        getData(`${base_url}/api-category/list-c/`, token).then(res => {
            dispatch({
                type: actions.getCategories,
                payload: res
            })
        })
    }
}

export const getFlavours = (token) => {
    return dispatch => {
        getData(`${base_url}/api-flavour/list-c/`, token).then(res => {
            dispatch({
                type: actions.getFlavours,
                payload: res
            })
        })
    }
}


export const onChange = (data) => {
    let type = ''
    switch (data.arrayToTarget) {
        case 'Brands':
            type = actions.onChangeBrand
            break

        case 'Categories':
            type = actions.onChangeCategory
            break

        case 'Flavours':
            type = actions.onChangeFlavour
            break

        default:
            type = ''
    }

    return dispatch => {
        dispatch({
            type,
            payload: data
        })
    }
}

export const addNew = (type, data) => {
    return dispatch => {
        dispatch({
            type,
            payload: data
        })
    }
}


// export const getKeyFetures = (token) => {
//     return dispatch => {

//         getData(`${base_url}/api-product/key-fetures-list-a/`, token).then(res => {
//             dispatch({
//                 type: actions.getKeyFetures,
//                 payload: res
//             })
//         })
//     }
// }