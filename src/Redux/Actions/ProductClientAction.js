import { actions } from '../AllActions'
import { base_url } from '../../Common/Constants'
import { getData } from '../../Common/apiCalls'

export const getProducts = (token) => {
    return dispatch => {
        getData(`${base_url}/api-product/list-a/`, token).then(res => {
            dispatch({
                type: actions.getProducts,
                payload: res
            })
        })
    }
}

export const getClients = (token) => {
    return dispatch => {
        getData(`${base_url}/api-client/list-create-a/`, token).then(res => {

            let data = {}
            res.forEach(element => {
                data[element.id] = element
            });

            dispatch({
                type: actions.getClients,
                payload: data
            })
        })
    }
}



export const onClientChange = (data) => {
    return dispatch => {
        dispatch({
            type: actions.onChangeClient,
            payload: data
        })
    }
}